from django.shortcuts import render
from .models import Project, API
from django.views import generic


def index(request):
    num_projects = Project.objects.all().count()

    return render(
        request,
        'index.html',
        context={'num_projects': num_projects},
    )


class ProjectListView(generic.ListView):
    model = Project


class ProjectDetailView(generic.DetailView):
    model = Project


class AllProjectsListView(ProjectListView):
    template_name = 'project_list'
    paginate_by = 5

    def get_queryset(self):
        return Project.objects.order_by('-last_updated')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['num_projects'] = Project.objects.all().count()
        return context


class MainPageView(ProjectListView):
    template_name = 'index.html'

    def get_queryset(self):
        return Project.objects.order_by('-last_updated')[:3]

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['num_projects'] = Project.objects.all().count()
        return context

# ===========================================


class APIListView(generic.ListView):
    model = API

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['num_apis'] = API.objects.all().count()
        return context


class APIDetailView(generic.DetailView):
    model = API
