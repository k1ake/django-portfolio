from django.urls import path
from . import views

urlpatterns = [
    path('', views.MainPageView.as_view(), name="index"),
    path('projects/', views.AllProjectsListView.as_view(), name="project-list"),
    path('project/<pk>/', views.ProjectDetailView.as_view(), name="project-detail"),
    path('apis/', views.APIListView.as_view(), name="api-list"),
    path('api-info/<pk>/', views.APIDetailView.as_view(), name="api-usage"),
]
