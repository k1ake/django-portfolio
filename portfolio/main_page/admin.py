from django.contrib import admin
from .models import Project, API


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'status', 'link', 'last_updated', 'date_started')
    list_filter = ('status', 'last_updated', 'date_started')
    fields = [('name', 'status'),
              'link',
              'last_updated',
              'date_started',
              'brief_description',
              'description',
              'readme_url',
              'forked_from']


class APIAdmin(admin.ModelAdmin):
    list_display = ('name', 'status')
    list_filter = ['status']
    fields = [('name', 'status', 'request_type'),
              'pretty_name',
              'brief_description',
              'usage']


admin.site.register(Project, ProjectAdmin)
admin.site.register(API, APIAdmin)
