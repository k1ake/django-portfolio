from django.db import models
from django.urls import reverse
import requests


class Project(models.Model):
    name = models.CharField(max_length=100, primary_key=True)
    description = models.TextField(max_length=10000, help_text="Project description", null=False, default="")
    brief_description = models.TextField(
        max_length=500, help_text="Brief project description for project", null=True, blank=True)
    link = models.CharField('link', max_length=100, null=True, blank=True)
    readme_url = models.CharField(max_length=100, null=True, blank=True)
    forked_from = models.CharField(max_length=100, null=True, blank=True)
    last_updated = models.DateTimeField(null=True, blank=True)
    date_started = models.DateField(null=True, blank=True)

    project_status = (
        ('a', 'Archived'),
        ('w', 'Work in progress'),
        ('d', 'Done'),
    )
    status = models.CharField(max_length=1, choices=project_status, default="w", help_text="Project status")

    def get_prettier_name(self):
        return self.name.replace("_", " ").capitalize()

    def set_readme_as_description(self):
        if self.readme_url:
            self.description = requests.get(self.readme_url.replace("/blob/", "/raw/")).text
        else:
            self.description = ""

    def get_absolute_url(self):
        return reverse('project-detail', args=[str(self.name)])

    class Meta:
        ordering = ["last_updated"]

    def __str__(self):
        return self.name


class API(models.Model):
    name = models.CharField(max_length=100, primary_key=True)
    pretty_name = models.CharField(max_length=100)
    usage = models.TextField(max_length=10000, help_text="API usage", null=True, blank=True)
    brief_description = models.TextField(
        max_length=500, help_text="Brief API description for APIs list", null=True, blank=True)

    request_type_choices = (
        ('GET', 'get'),
        ('POST', 'post'),
        ('PUT', 'put'),
        ('DELETE', 'delete'),
    )

    request_type = models.CharField(max_length=10, choices=request_type_choices, default="GET")

    api_status = (
        ('a', 'Abandoned'),
        ('w', 'Work in progress'),
        ('r', 'Ready to use'),
    )
    status = models.CharField(max_length=1, choices=api_status, default="w", help_text="Project status")

    def get_absolute_url(self):
        return reverse('api-usage', args=[str(self.name)])

    def __str__(self):
        return self.name
