from rest_framework.response import Response
from rest_framework.decorators import api_view
from main_page.models import Project
from .serializers import ProjectSerializer
from .API_methods.retrieve_projects import GitLabProjects
from django.conf import settings


@api_view(["GET"])
def getProjects(request):
    projects = Project.objects.all()
    serializer = ProjectSerializer(projects, many=True)
    return Response(serializer.data)


@api_view(["GET"])
def updateDB(request):
    present_projects = Project.objects.all()
    in_db = [p.link for p in present_projects]
    gitlab_projects = GitLabProjects(token=settings.GITLAB_TOKEN, user_id=settings.GITLAB_ID).get_projects_dict()
    gitlab_projects_links = list(gitlab_projects.keys())
    not_in_db = [p for p in gitlab_projects_links if p not in in_db]

    if not_in_db:
        for url, gp in gitlab_projects.items():
            if url in not_in_db:
                gp["date_started"] = gp["date_started"].split("T")[0]
                p = {key: value for key, value in gp.items() if key != "archived"}
                new_p = Project(**p)
                if new_p.readme_url:
                    new_p.set_readme_as_description()
                if gp["archived"]:
                    new_p.status = "a"
                new_p.save()

    for p in present_projects:
        p.name = gitlab_projects[p.link]["name"]
        p.brief_description = gitlab_projects[p.link]["brief_description"]
        p.readme_url = gitlab_projects[p.link]["readme_url"]
        p.forked_from = gitlab_projects[p.link]["forked_from"]
        p.last_updated = gitlab_projects[p.link]["last_updated"]
        p.date_started = gitlab_projects[p.link]["date_started"].split("T")[0]

        if gitlab_projects[p.link]["archived"]:
            p.status = "a"
        p.save()

    answer = {"added_to_db": not_in_db,
              "updated": [p.link for p in present_projects]}
    return Response(answer)
