from django.apps import AppConfig


class MyApisConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'my_apis'
