import gitlab


class GitLabProjects:
    def __init__(self, token: str, user_id: str):
        self.gl = gitlab.Gitlab(private_token=token)
        self.user = self.gl.users.get(id=user_id)
        self.projects_full = self.user.projects.list(iterator=True)
        self.projects = [self.project_info(i.attributes) for i in self.projects_full]

    def project_info(self, project: dict) -> dict:
        answer = {
            "name": project["name"].lower(),
            "brief_description": project["description"],
            "link": project["web_url"],
            "archived": project["archived"],
            "readme_url": project["readme_url"],
            "forked_from": project.get("forked_from_project", "not forked"),
            "date_started": project["created_at"],
            "last_updated": project["last_activity_at"],
        }
        if type(answer["forked_from"]) is dict:
            answer["forked_from"] = answer["forked_from"].get("web_url")
        return answer

    def get_projects(self) -> list:
        return self.projects

    def get_projects_dict(self) -> dict:
        ps = {p["link"]: p for p in self.projects}
        return ps
