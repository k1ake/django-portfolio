# Portfolio site made with django

<!--
Полезные ссылки для разработки
Учебник по джанго от мозиллы
https://developer.mozilla.org/ru/docs/Learn/Server-side/Django
Доки джанго от джанго :D
https://docs.djangoproject.com/en/4.2/intro/
python-gitlab доки
https://python-gitlab.readthedocs.io/en/stable/index.html
Расширения для python-markdown
https://python-markdown.github.io/extensions/
-->
## Предварительный набросок структуры сайта

Главная страничка (/):

- Последние 3 проекта

Библиотека (/projects/):

- Список со всеми проектами

Апишки (/apis/):

- Список всех забабаханных api с помошью джанго

## Frontend

В планах попробовать React

## API

1. projects - выдаёт жсон с названиями проектов, датой последнего изменения и ссылкой
2. updatedb - подхватывает проекты из гитлаба и обновляет базу
3. ___ - tbd

## TODO

- [x] Отрисовывать markdown на странице с детальной информацией по проекту
- [ ] Для каждого API сделать свой markdown для поля usage
- [x] Сделать парсинг гитлабовских проектов в бд
- [ ] Сделать красиво
