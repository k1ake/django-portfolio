import gitlab, datetime
with open("key.xd") as f:
    lines = f.readlines()
    GITLAB_TOKEN = lines[1].replace("\n", "")
    GITLAB_ID = lines[2].replace("\n", "")


def project_info(project: dict) -> dict:
    answer = {
        "name": project["name"],
        "creation_date": project["created_at"],
        "last_update": project["last_activity_at"],
        "tag_list": project["tag_list"],
        "topics": project["topics"],
        "readme_url": project["readme_url"],
        "forked_from": project.get("forked_from_project", "not forked"),
        "visibility": project["visibility"],
        "archived": project["archived"]
    }
    if type(answer["forked_from"]) is dict:
        answer["forked_from"] = answer["forked_from"].get("web_url")

    date_started = datetime.datetime.strptime(answer["creation_date"].split("T")[0], "%Y-%m-%d").date()
    print(date_started)
    return answer


def main():
    gl = gitlab.Gitlab(private_token=GITLAB_TOKEN)

    user = gl.users.get(id=GITLAB_ID)
    projects = user.projects.list(iterator=True)
    pps = [project_info(i.attributes) for i in projects]
    for p in pps:
        print(type(p["creation_date"]))


if __name__ == "__main__":
    main()
