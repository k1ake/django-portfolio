import requests, markdown

r = requests.get("https://gitlab.com/k1ake/passman/-/raw/main/README.md")

# with open("new.md", "w") as f:
#     f.write(r.text)

with open("test.html", "w") as f:
    f.write(markdown.markdown(r.text, extensions=['fenced_code', 'tables']))
